# README

## Transfer Learning on Facial Expression Recognition 2013 (FER-2013)

## Description
In this short project, we are going to train a neural network model on the Facial Expression Recognition 2013 (FER-2013) dataset.

The task is to classify facial expressions from 35,887 examples of 48x48 pixel grayscale images of faces. 
Images are categorized based on the emotion shown in the facial expressions (happiness, neutral, sadness, anger, surprise, disgust, fear).

The network attempts to apply transfer learning making use of the LeNet model which in turn is trained on the MNIST dataset. 

## Project status
The LeNet model trained on MNIST constituted a previous project of mine. This is a project inspired by a Maths course for Busineess Economics at Vrije Universiteit Brussel and is based on personal interest. The requirement was to perform any action on any dataset using Wolfram. The final neural network is not good enough for any serious purposes and is rather a learning project that allowed to have a look at transfer learning as a concept and Wolfram as a language.
